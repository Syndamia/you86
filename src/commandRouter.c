#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <strings.h>
#include "commandHandler.h"

extern struct CommandHandler handlers[MAX_HANDLERS];
extern int handlerCount;

bool route(char *p_command) {
	p_command = strtok(p_command, " ");

	for (int i = 0; i < handlerCount; i++) {
		if (strcasecmp(handlers[i].p_name, p_command) == 0) {
			handlers[i].p_executor(p_command);
			return true;
		}
	}
	return false;
}
