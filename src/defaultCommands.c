#include "commandHandler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help(char *p_args) {
	printf("This is the help message");
}

void about(char *p_args) {
	printf("This is the about message");
}

void version(char *p_args) {
	printf("This is the version message");
}

void exitApp(char *p_args) {
	exit(EXIT_SUCCESS);
}

void test(char *p_args) {
	int i = 0;
	while (p_args != NULL) {
		printf("Argument %d: %s\n", i++, p_args);
		p_args = strtok(NULL, " ");
	}
}

extern void registerHandler(char *, P_EXECUTOR);

void registerDefaultHandlers() {
	registerHandler("help", &help);
	registerHandler("h", &help);
	registerHandler("?", &help);
	registerHandler("about", &about);
	registerHandler("a", &about);
	registerHandler("version", &version);
	registerHandler("v", &version);
	registerHandler("exit", &exitApp);
	registerHandler("e", &exitApp);
	registerHandler("quit", &exitApp);
	registerHandler("q", &exitApp);

	registerHandler("t", &test);
}
