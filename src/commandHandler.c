#include "commandHandler.h"

struct CommandHandler handlers[MAX_HANDLERS];

int handlerCount = 0;

void registerHandler(char *p_name, P_EXECUTOR p_executor) {
	struct CommandHandler newCH;
	newCH.p_name = p_name;
	newCH.p_executor = p_executor;

	handlers[handlerCount++] = newCH;
}
