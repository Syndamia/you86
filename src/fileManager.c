/* .
 * L__ Procedures
 * |   L__ 1h_workout.you86
 * |   L__ 15m_workout.you86
 * |   L__ math_homework.you86
 * |   L__ grocery_shopping.you86
 * |
 * L__ DailyStack
 *     L__ 02-09-2021
 *         L__ ParentProcedures.you86
 *         L__ result-1h_workout.png
 *         L__ result-math_homework.docx
 */
#include <stdbool.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define StrSize(str) (sizeof(str)/sizeof(str[0]))
#define StrCpyCat(dest, cpy, cat) ({\
			strcpy(dest, cpy);\
			strcat(dest, cat);\
		})

#define ROOT_DIR "/Documents/You86/"
#define PROCEDURES_DIR ROOT_DIR"Procedures/"
#define DAILY_STACK_DIR ROOT_DIR"DailyStack/"

char today[11];

void createDir(char *);

void initTodayString() {
	time_t t = time(NULL);
	struct tm localTime = *localtime(&t);
	snprintf(today, StrSize(today), "%02d-%02d-%d", localTime.tm_mday, localTime.tm_mon + 1, localTime.tm_year + 1900);
}

void initFiles() {
	createDir(ROOT_DIR);
	createDir(PROCEDURES_DIR);
	createDir(DAILY_STACK_DIR);

	initTodayString();

	char todayPath[strlen(DAILY_STACK_DIR) + 11];
	StrCpyCat(todayPath, DAILY_STACK_DIR, today);

	createDir(todayPath);
}

void createDir(char *p_path) {
	char *p_tilde = getenv("HOME");
	int tildeLen = strlen(p_tilde), pathLen = strlen(p_path);

	char fullPath[tildeLen + pathLen];
	StrCpyCat(fullPath, p_tilde, p_path);

	int ret = mkdir(fullPath, S_IRWXU);
	if (ret == -1) {
		switch (errno) {
			case EEXIST:
				return;
			case EACCES :
				printf("The parent directory does not allow write for %s", fullPath);
				break;
			case ENAMETOOLONG:
				printf("Pathname \"%s\" is too long", fullPath);
				break;
			default:
				perror("mkdir");
				printf("Path: \"%s\"\n", fullPath);
				break;
		}
		exit(EXIT_FAILURE);
	}
}
