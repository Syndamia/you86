#ifndef COMMAND_HANDLER_HEADER 
#define COMMAND_HANDLER_HEADER

#define MAX_HANDLERS 32

typedef void (*P_EXECUTOR)(char *);

struct CommandHandler {
	char *p_name;
	P_EXECUTOR p_executor;
};

#endif
