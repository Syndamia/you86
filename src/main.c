#include <bits/posix2_lim.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define SHELL_STRING "> "

extern void initFiles();
extern void registerDefaultHandlers();

extern bool route(char *command);
void printDefault();

int main() {
	initFiles();
	registerDefaultHandlers();

	char buffer[LINE_MAX];
	buffer[0] = 0;

	printf("Welcome!\n\n");
	do {
		// Removes the Line Feed character at the "end", if it exists
		char *p_toChange = strchr(buffer, 0x0A);
		if (p_toChange != NULL)
			p_toChange[0] = 0;

		if (buffer[0] > 0)
			if (route(buffer))
				printf("\n");
		
		printf(SHELL_STRING);
	} while (fgets(buffer, LINE_MAX, stdin));

	return 0;
}
